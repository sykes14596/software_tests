matrixSize = 4000;
gpuDevice([]);
nGPUs = gpuDeviceCount();
p = gcp;
spmd
  gd = gpuDevice;
  idx = gd.Index;
  disp(['Using GPU ',num2str(idx)]);
end

% 5 GPUs
time5GPUs = 0;
parfor i = 1:p.NumWorkers
    gd = gpuDevice;
    XGs = rand(matrixSize,'gpuArray');
    XGs_A = XGs * XGs;
    XGs_B = XGs / XGs;
    XGs_C = @() bsxfun(@times, XGs_A, XGs_B);
    wait(gd);
    time5GPUs = time5GPUs + gputimeit(XGs_C);
end

% 1 GPU
time1GPU = 0;
parfor i = 1:p.NumWorkers
    XG = rand(matrixSize,'gpuArray');
    XG_A = XG * XG;
    XG_B = XG / XG;
    XG_C = @() bsxfun(@times, XG_A, XG_B);
    time1GPU = time1GPU+gputimeit(XG_C);
end


% CPU
timeCPU = 0;
for i = 1:p.NumWorkers
    X = rand(matrixSize);
    X_A = X * X;
    X_B = X / X;
    X_C = @() bsxfun(@times, X_A, X_B);
    timeCPU = timeCPU+ timeit(X_C);
end



speedUp1 = timeCPU / time1GPU;
speedUp5 = timeCPU / time5GPUs;

fprintf('\n\n');
disp(['Execution time on CPU = ',num2str(timeCPU)]);
disp(['Execution time on 1 GPU = ',num2str(time1GPU)]);
disp(['Execution time on 5 GPU = ',num2str(time5GPUs)]);

disp(['Speedup (CPUtime / 1 GPUtime) = ', num2str(speedUp1)]);
disp(['Speedup (CPUtime / 5 GPUtime) = ', num2str(speedUp5)]);





